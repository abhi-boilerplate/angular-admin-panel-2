export interface CardInfo {
    name?: string;
    company?: string;
    image?: File;
    email?: string;
    mobile?: string;
    state?: string;
    city?: string;
    address?: string;
}