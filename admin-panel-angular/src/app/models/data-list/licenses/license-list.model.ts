export interface LicenseListModel {
    id?: string;
    image?: string;
    licenseNo?: string;
    name?: string;
    email?: string;
    phone?: string;
    companyName?: string;
    companyMobile?: string;
    licenseType?: string;
    islock?: string;
    isActive?: string;
}