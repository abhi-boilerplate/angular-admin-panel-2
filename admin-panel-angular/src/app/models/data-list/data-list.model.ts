import { ListTypeEnum } from "src/app/enums/list-type.enum";


export interface ListRecord {
    name: string;
    className: string;
    listModel: ListTypeEnum;
}
export class ListItems {
    items: ListChilds[];
}

export class ListChilds {
    constructor(public name: string, public className: string, public listModel: ListTypeEnum){}
   
}