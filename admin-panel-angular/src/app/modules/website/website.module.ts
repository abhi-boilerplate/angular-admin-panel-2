import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page/home-page.component';
import { AboutPageComponent } from './about-page/about-page.component';
import { WebsiteRoutingModule } from './website.routing.module';
import { ContactPageComponent } from './contact-page/contact-page.component';
import { PricingPageComponent } from './pricing-page/pricing-page.component';
import { MasterPageComponent } from './master-page/master-page.component';
import { RouterModule } from '@angular/router';
import { ProductPageComponent } from './product-page/product-page/product-page.component';



@NgModule({
  declarations: [
    HomePageComponent, 
    AboutPageComponent, 
    ContactPageComponent,
    PricingPageComponent,
    ProductPageComponent,
    MasterPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    WebsiteRoutingModule
  ]
})
export class WebsiteModule { }
