import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AboutPageComponent } from "./about-page/about-page.component";
import { ContactPageComponent } from "./contact-page/contact-page.component";
import { HomePageComponent } from "./home-page/home-page.component";
import { MasterPageComponent } from "./master-page/master-page.component";
import { PricingPageComponent } from "./pricing-page/pricing-page.component";
import { ProductPageComponent } from "./product-page/product-page/product-page.component";

const routes: Routes = [{
    path: '',
    component: MasterPageComponent,
    children: [{
        path: 'home',
        component: HomePageComponent
    },
    {
        path: 'about',
        component: AboutPageComponent
    },
    {
        path: 'contact',
        component: ContactPageComponent
    },
    {
        path: 'product',
        children: [{
            path: 'pro1',
            component: ProductPageComponent
        }]
    },
    {
        path: 'product/:id',
        component: PricingPageComponent
    }
    ]

}];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class WebsiteRoutingModule {

}