import { Component, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'sa-agent-list',
  templateUrl: './agent-list.component.html',
  styleUrls: ['./agent-list.component.scss']
})
export class AgentListComponent implements OnInit {

  constructor(private render: Renderer2) { }

  ngOnInit(): void {
  }

  public actionHover(dropDown: any): void {
    const active = dropDown.classList.contains('is-active');
    this.render[active ? 'removeClass' : 'addClass'](dropDown, 'is-active');
  }

}
