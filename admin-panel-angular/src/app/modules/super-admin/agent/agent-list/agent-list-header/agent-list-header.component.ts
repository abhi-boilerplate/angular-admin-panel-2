import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sa-agent-list-header',
  templateUrl: './agent-list-header.component.html',
  styleUrls: ['./agent-list-header.component.scss']
})
export class AgentListHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
