import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentListHeaderComponent } from './agent-list-header.component';

describe('AgentListHeaderComponent', () => {
  let component: AgentListHeaderComponent;
  let fixture: ComponentFixture<AgentListHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgentListHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentListHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
