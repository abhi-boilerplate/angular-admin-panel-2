import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorPageComponent } from 'src/app/shared/error-page/error-page.component';
import { AgentListComponent } from './agent/agent-list/agent-list/agent-list.component';
import { ArchiveComponent } from './archive/archive/archive.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { DefaultComponent } from './dashboard/default/default.component';
import { PaymentGatewayComponent } from './gateway/payment-gateway/payment-gateway.component';
import { LicenseDetailComponent } from './licenses/license-detail/license-detail/license-detail.component';
import { LicenseNewComponent } from './licenses/license-new/license-new/license-new.component';
import { LicenseComponent } from './licenses/license/license.component';
import { DiscountsComponent } from './reporting/discounts/discounts.component';
import { PaymentsComponent } from './reporting/payments/payments.component';
import { SmsComponent } from './reporting/sms/sms.component';
import { ValidityComponent } from './reporting/validity/validity.component';

const routes: Routes = [
    {

        path: 'sa',
        component: DefaultComponent,
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'license',
                children: [
                    {
                        path: '',
                        component: LicenseComponent
                    },
                    {
                        path: 'new',
                        component: LicenseNewComponent
                    }, 
                    {
                        path:'detail/:id',
                        component:LicenseDetailComponent
                    }
                ]
            },
            {
                path: 'agent',
                children: [
                    {
                        path: '',
                        component: AgentListComponent
                    },
                ]
            },
            {
                path: 'archive',
                component: ArchiveComponent
            },
            {
                path: 'sms',
                component: SmsComponent
            },
            {
                path: 'validity',
                component: ValidityComponent
            },
            {
                path: 'payment',
                component: PaymentsComponent
            },
            {
                path: 'discount',
                component: DiscountsComponent
            },
            {
                path: 'gate',
                component: PaymentGatewayComponent
            }
        ],
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class SuperAdminRoutingModule { }
