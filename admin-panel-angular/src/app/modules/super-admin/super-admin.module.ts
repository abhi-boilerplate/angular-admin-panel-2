import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { TilesComponent } from './dashboard/tiles/tiles.component';
import { DefaultComponent } from './dashboard/default/default.component';
import { SuperAdminRoutingModule } from './super-admin-routing.module';
import { LicenseComponent } from './licenses/license/license.component';
import { ArchiveComponent } from './archive/archive/archive.component';
import { SmsComponent } from './reporting/sms/sms.component';
import { ValidityComponent } from './reporting/validity/validity.component';
import { PaymentsComponent } from './reporting/payments/payments.component';
import { DiscountsComponent } from './reporting/discounts/discounts.component';
import { PaymentGatewayComponent } from './gateway/payment-gateway/payment-gateway.component';
import { SideNavComponent } from './dashboard/side-nav/side-nav.component';
import { LicenseHeaderComponent } from './licenses/license-header/license-header.component';
import { LicenseListComponent } from './licenses/license-list/license-list.component';
import { LicenseEditComponent } from './licenses/license-edit/license-edit.component';
import { LicenseCreateComponent } from './licenses/license-create/license-create.component';
import { EllipsisPipe } from 'src/app/utilities/helpers/ellipsis.helper';
import { LicenseCardInfoComponent } from './licenses/license-new/license-card-info/license-card-info.component';
import { LicenseNewFormComponent } from './licenses/license-new/license-new-form/license-new-form.component';
import { LicensePricingComponent } from './licenses/license-new/license-pricing/license-pricing.component';
import { LicenseNewComponent } from './licenses/license-new/license-new/license-new.component';
import { LicenseModalComponent } from './licenses/license-new/license-modal/license-modal.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ImagePipe } from 'src/app/utilities/pipes/image.pipe';
import { LicenseDetailComponent } from './licenses/license-detail/license-detail/license-detail.component';
import { LicenseDetailUserCardComponent } from './licenses/license-detail/license-detail-user-card/license-detail-user-card.component';
import { LicenseInfoComponent } from './licenses/license-detail/sections/license-info/license-info.component';
import { ListUsersComponent } from './licenses/license-detail/sections/list-users/list-users.component';
import { LicenseValidityComponent } from './licenses/license-detail/sections/license-validity/license-validity.component';
import { SmsDetailComponent } from './licenses/license-detail/sections/sms-detail/sms-detail.component';
import { UserDetailsComponent } from './licenses/license-detail/sections/user-details/user-details.component';
import { SaleDetailsComponent } from './licenses/license-detail/sections/sale-details/sale-details.component';
import { PaymentDetailsComponent } from './licenses/license-detail/sections/payment-details/payment-details.component';
import { DiscountDetailsComponent } from './licenses/license-detail/sections/discount-details/discount-details.component';
import { UserLedgerComponent } from './licenses/license-detail/sections/user-ledger/user-ledger.component';
import { InvoicesComponent } from './licenses/license-detail/sections/invoices/invoices.component';
import { AgentListComponent } from './agent/agent-list/agent-list/agent-list.component';
import { AgentListHeaderComponent } from './agent/agent-list/agent-list-header/agent-list-header.component';

@NgModule({
  declarations: [
    TilesComponent,
    DefaultComponent,
    LicenseComponent,
    ArchiveComponent,
    SmsComponent,
    ValidityComponent,
    EllipsisPipe,
    PaymentsComponent,
    DiscountsComponent,
    SideNavComponent,
    PaymentGatewayComponent,
    LicenseHeaderComponent,
    LicenseListComponent,
    LicenseEditComponent,
    LicenseCreateComponent,
    LicenseCardInfoComponent,
    LicenseNewFormComponent,
    LicensePricingComponent,
    LicenseNewComponent,
    LicenseModalComponent,
    ImagePipe,
    LicenseDetailComponent,
    LicenseDetailUserCardComponent,
    LicenseInfoComponent,
    ListUsersComponent,
    LicenseValidityComponent,
    SmsDetailComponent,
    UserDetailsComponent,
    SaleDetailsComponent,
    PaymentDetailsComponent,
    DiscountDetailsComponent,
    UserLedgerComponent,
    InvoicesComponent,
    AgentListComponent,
    AgentListHeaderComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,
    SuperAdminRoutingModule
  ],
  exports: [DefaultComponent]
})
export class SuperAdminModule { }
