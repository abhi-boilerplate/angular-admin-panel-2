import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';

@Component({
  selector: 'sa-license-modal',
  templateUrl: './license-modal.component.html',
  styleUrls: ['./license-modal.component.scss']
})
export class LicenseModalComponent implements OnInit {


  @ViewChild('paymentModal', { static: false })
  paymentModal: ElementRef;

  constructor(private render: Renderer2) { }

  ngOnInit(): void {
  }


  closeModal() {
    this.render.removeClass(this.paymentModal.nativeElement, "is-active");
  }

  openModal(){
    this.render.addClass(this.paymentModal.nativeElement, "is-active");
  }

}
