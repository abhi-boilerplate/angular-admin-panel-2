import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseCardInfoComponent } from './license-card-info.component';

describe('LicenseCardInfoComponent', () => {
  let component: LicenseCardInfoComponent;
  let fixture: ComponentFixture<LicenseCardInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LicenseCardInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseCardInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
