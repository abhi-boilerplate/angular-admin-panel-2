import { Component, Input, OnInit } from '@angular/core';
import { CardInfo } from 'src/app/models/data-list/licenses/card-info.interface';

@Component({
  selector: 'sa-license-card-info',
  templateUrl: './license-card-info.component.html',
  styleUrls: ['./license-card-info.component.scss']
})
export class LicenseCardInfoComponent implements OnInit {

  public selectedImage: string | ArrayBuffer = "https://bulma.io/images/placeholders/128x128.png";

  @Input()
  public card: CardInfo;
  constructor() { }

  ngOnInit(): void {
  }

  onFileChanged(event) {
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    this.card.image = event.target.files[0];
    reader.onload = (event) => {
      this.selectedImage = event.target.result;
    }
  }

}
