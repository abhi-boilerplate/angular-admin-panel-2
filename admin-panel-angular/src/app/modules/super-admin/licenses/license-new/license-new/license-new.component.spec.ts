import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseNewComponent } from './license-new.component';

describe('LicenseNewComponent', () => {
  let component: LicenseNewComponent;
  let fixture: ComponentFixture<LicenseNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LicenseNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
