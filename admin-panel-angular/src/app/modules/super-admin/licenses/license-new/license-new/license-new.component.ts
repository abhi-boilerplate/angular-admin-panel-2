import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { CardInfo } from 'src/app/models/data-list/licenses/card-info.interface';
import { LicenseModalComponent } from '../license-modal/license-modal.component';
import { LicenseNewFormComponent } from '../license-new-form/license-new-form.component';

@Component({
  selector: 'sa-license-new',
  templateUrl: './license-new.component.html',
  styleUrls: ['./license-new.component.scss']
})
export class LicenseNewComponent implements OnInit {

  @ViewChild("paymentModal", { static: false })
  public paymentModal: LicenseModalComponent;

  @ViewChild("licenseDetail", { static: false })
  private licenseDetail: LicenseNewFormComponent;

  @ViewChild("priceDetail", { static: false })
  private priceDetail: LicenseNewFormComponent;

  public card: CardInfo = {};


  public lockForm = false;

  constructor() { }

  ngOnInit(): void {
  }

  public openPaymentModal(event: boolean): void {
    console.log(event);
    if (event)
      this.paymentModal.openModal();
  }


  public lockFormDetail(event: boolean): void {
   console.log('entered');
    this.lockForm = event;
      this.priceDetail.formDisable = !event;
    if (!event)
      this.licenseDetail.formDisable = false;

      console.log(this.priceDetail.formDisable);
  }

  public textChange(event: CardInfo) {
    this.card = event;
  }



}
