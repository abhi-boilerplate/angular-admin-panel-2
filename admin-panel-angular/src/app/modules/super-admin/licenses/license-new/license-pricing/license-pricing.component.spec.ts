import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LicensePricingComponent } from './license-pricing.component';

describe('LicensePricingComponent', () => {
  let component: LicensePricingComponent;
  let fixture: ComponentFixture<LicensePricingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LicensePricingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LicensePricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
