import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'sa-license-pricing',
  templateUrl: './license-pricing.component.html',
  styleUrls: ['./license-pricing.component.scss']
})
export class LicensePricingComponent implements OnInit {

  @Output()
  public payAmoutEvent = new EventEmitter<boolean>();

  @Output()
  public cancelPaymentEvent = new EventEmitter<boolean>();

  public formDisable = false;

  public licenseBuy = 1;

  constructor() { }

  ngOnInit(): void {
  }



  public changeLicense(btnText: string): void {
    btnText === 'add' ? ++this.licenseBuy : --this.licenseBuy;
  }


  public payAmount(): void {
    this.payAmoutEvent.emit(true);
  }

  public cancelPayment(): void {
    console.log('cancel')
    this.cancelPaymentEvent.emit(true);
  }

}
