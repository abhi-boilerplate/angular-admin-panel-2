import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseNewFormComponent } from './license-new-form.component';

describe('LicenseNewFormComponent', () => {
  let component: LicenseNewFormComponent;
  let fixture: ComponentFixture<LicenseNewFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LicenseNewFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseNewFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
