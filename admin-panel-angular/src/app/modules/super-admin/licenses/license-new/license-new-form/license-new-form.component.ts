import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CardInfo } from 'src/app/models/data-list/licenses/card-info.interface';

@Component({
  selector: 'sa-license-new-form',
  templateUrl: './license-new-form.component.html',
  styleUrls: ['./license-new-form.component.scss']
})
export class LicenseNewFormComponent implements OnInit {

  @Output()
  public lockForm = new EventEmitter<boolean>();

  @Output()
  public cardInfoEvent = new EventEmitter<CardInfo>();

  public formDisable = false;

  public licenseForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.licenseForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      mobile: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      company: ['', [Validators.required, Validators.minLength(3)]],
      companyEmail: ['', [Validators.required, Validators.email]],
      companyPhone: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      state: [''],
      city: [''],
      address: ['']
    });
  }

  ngOnInit(): void {
  }

  public lockDetails(): void {
    this.lockForm.emit(true);
    this.formDisable = true;
  }

  public textChange() {
    this.cardInfoEvent.emit({
      company: this.licenseForm.get('company').value,
      email: this.licenseForm.get('email').value,
      mobile: this.licenseForm.get('mobile').value,
      name: this.licenseForm.get('name').value,
      address: this.licenseForm.get('address').value,
      city: this.licenseForm.get('city').value,
      state: this.licenseForm.get('state').value,
    })
  }


}
