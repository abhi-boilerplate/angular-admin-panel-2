import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ListTypeEnum } from 'src/app/enums/list-type.enum';
import { ListRecord } from 'src/app/models/data-list/data-list.model';
import { LicenseListModel } from 'src/app/models/data-list/licenses/license-list.model';
import { LicenseMockData } from 'src/app/utilities/mocks/licenses-mock';
import { ToastService } from 'src/app/utilities/helper-services/toast-service.service';
@Component({
  selector: 'sa-license-list',
  templateUrl: './license-list.component.html',

  styleUrls: ['./license-list.component.scss']
})
export class LicenseListComponent implements OnInit {

  public selectedList: LicenseListModel = {};

  listHeader: ListRecord[] = [];
  listItem: LicenseListModel[] = [];


  @ViewChild('deleteModal', { static: false })
  deleteModal: ElementRef;


  @ViewChild('updateModel', { static: false })
  updateModel: ElementRef;

  constructor(private render: Renderer2, private toast: ToastService) { }

  ngOnInit(): void {
    this.listItem = LicenseMockData;
    this.createTableHeader();
  }

  private createTableHeader() {
    this.listHeader = [
      { name: '', className: 'column is-0 mx-2 mr-3 my-1 p-1', listModel: ListTypeEnum.Text },
      { name: 'License No', className: 'column is-1 mx-2 my-1 p-1', listModel: ListTypeEnum.Text },
      { name: 'Name', className: 'column is-1 mx-2 my-1 p-1', listModel: ListTypeEnum.Text },
      { name: 'Email', className: 'column is-1 mx-2 my-1 p-1', listModel: ListTypeEnum.Text },
      { name: 'Phone', className: 'column is-1 mx-2 my-1 p-1', listModel: ListTypeEnum.Text },
      { name: 'Company Name', className: 'column is-2 mx-2 my-1 p-1', listModel: ListTypeEnum.Text },
      { name: 'C. Mobile', className: 'column is-1 mx-2 my-1 p-1', listModel: ListTypeEnum.Text },
      { name: 'Lic Type', className: 'column is-small mx-2 my-1 p-1', listModel: ListTypeEnum.Text },
      { name: 'Locked', className: 'column is-small mx-2 my-1 p-1', listModel: ListTypeEnum.Text },
      { name: 'Active', className: 'column is-small mx-2 my-1 p-1', listModel: ListTypeEnum.Text },
      { name: '', className: 'column is-small mx-2 my-1 p-1', listModel: ListTypeEnum.Text }
    ];

  }
  public actionHover(dropDown: any): void {
    const active = dropDown.classList.contains('is-active');
    this.render[active ? 'removeClass' : 'addClass'](dropDown, 'is-active');
  }

  public deleteLicense(record: any): void {
    this.selectedList = record;
    this.render.addClass(this.deleteModal.nativeElement, 'is-active');
  }

  public closeModal(modal: string): void {
    this.selectedList = {};
    if (modal === 'delete')
      this.render.removeClass(this.deleteModal.nativeElement, 'is-active');
    if (modal === 'update')
      this.render.removeClass(this.updateModel.nativeElement, 'is-active');
  }

  public deleteLicenseModal(): void {
    this.listItem.splice(this.listItem.indexOf(this.selectedList), 1)
    this.toast.info(this.selectedList.name + " account is deleted successfully", "Information");
    this.closeModal('delete');
  }

  public updateLicenseModal(item: LicenseListModel): void {
    this.selectedList = item;
    this.render.addClass(this.updateModel.nativeElement, 'is-active');
  }
}
