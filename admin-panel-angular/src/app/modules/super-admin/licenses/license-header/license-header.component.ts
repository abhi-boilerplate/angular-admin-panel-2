import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'sa-license-header',
  templateUrl: './license-header.component.html',
  styleUrls: ['./license-header.component.scss']
})
export class LicenseHeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

 }
