import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseValidityComponent } from './license-validity.component';

describe('LicenseValidityComponent', () => {
  let component: LicenseValidityComponent;
  let fixture: ComponentFixture<LicenseValidityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LicenseValidityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseValidityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
