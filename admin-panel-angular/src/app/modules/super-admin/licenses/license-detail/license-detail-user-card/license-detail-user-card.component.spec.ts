import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseDetailUserCardComponent } from './license-detail-user-card.component';

describe('LicenseDetailUserCardComponent', () => {
  let component: LicenseDetailUserCardComponent;
  let fixture: ComponentFixture<LicenseDetailUserCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LicenseDetailUserCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseDetailUserCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
