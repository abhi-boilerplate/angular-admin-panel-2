import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  
  public loginForm: FormGroup;


  constructor(private fb: FormBuilder) {
    this.loginForm=this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.min(5)]]
    })
   }

  ngOnInit(): void {
  }


  submitForm() {
   console.log(this.loginForm.value);
   this.loginForm.reset();
  }

}
