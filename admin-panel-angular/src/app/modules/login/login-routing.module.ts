import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForgetPageComponent } from './forget-page/forget-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
    {

        path: 'login',
        component: LoginComponent,
        children: [
            {
                path: '',
                component: LoginPageComponent
            },
            {
                path: 'forget',
                component: ForgetPageComponent
            },
        ],
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class LoginRoutingModule { }
