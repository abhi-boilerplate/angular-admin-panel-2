import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'imagePipe'
})

export class ImagePipe implements PipeTransform {
    selectedImage: string | ArrayBuffer;
    transform(value: any, ...args: any[]) {
        console.log(value);
        if (value) {
            const reader = new FileReader();
            reader.readAsDataURL(value);
            reader.onload = (event) => {
                console.log(event.target.result);
                this.selectedImage = event.target.result;
            }
            return this.selectedImage;
        }
        return 'https://bulma.io/images/placeholders/128x128.png';
    }
}