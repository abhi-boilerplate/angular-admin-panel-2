import { LicenseListModel } from "src/app/models/data-list/licenses/license-list.model";

export const LicenseMockData: LicenseListModel[] = [{
    id: "55",
    name: 'Abhishek Raheja',
    licenseNo: '123456789',
    licenseType: 'Registerd',
    companyMobile: '9914964911',
    companyName: 'Raheja Communication',
    email:'raheja.abhi@hotmail.com',
    image: 'https://cdn4.iconfinder.com/data/icons/avatars-xmas-giveaway/128/batman_hero_avatar_comics-512.png',
    isActive:'YES',
    islock:'NO',
    phone:'9914964911'
}]