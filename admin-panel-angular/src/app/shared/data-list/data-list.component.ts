import { Component, Input, OnInit } from '@angular/core';
import { ListTypeEnum } from 'src/app/enums/list-type.enum';
import { ListItems, ListRecord } from 'src/app/models/data-list/data-list.model';

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.scss']
})
export class DataListComponent implements OnInit {

  @Input()
  listHeader: ListRecord[] = [];

  @Input()
  listItems: ListItems[] = [];

  public listType: typeof ListTypeEnum = ListTypeEnum;

  constructor() { }

  ngOnInit(): void {

  }



}
