import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @ViewChild('dropDownView', { static: false })
  public dropDownView: ElementRef

  constructor(private render: Renderer2) { }

  ngOnInit(): void {
  }


  public menuExpend() {
    const active = this.dropDownView.nativeElement.classList.contains('is-active');
    this.render[active ? 'removeClass' : 'addClass'](this.dropDownView.nativeElement, 'is-active');

  }

}
