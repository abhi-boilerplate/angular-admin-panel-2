import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { RouterModule } from '@angular/router';
import { DataListComponent } from './data-list/data-list.component';

@NgModule({
  declarations: [HeaderComponent, FooterComponent, ErrorPageComponent,  DataListComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [HeaderComponent, FooterComponent, ErrorPageComponent, DataListComponent],
})
export class SharedModule { }
